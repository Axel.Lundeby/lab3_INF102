package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

    @Override
    public int findNumber(RandomNumber number) {
        int upperInt = 10000;
        int lowerInt = 0;

        int guessNumber = 5000;
        return guessNumber(number, guessNumber, upperInt, lowerInt);
    }

    private int guessNumber(RandomNumber number, int guessedNumber, int upperInt, int lowerInt) {
        int result = number.guess(guessedNumber);

        if (-1 == result) {
            return guessNumber(number, (guessedNumber + upperInt) / 2, upperInt, guessedNumber);
        }
        if (1 == result) {
            return guessNumber(number, (guessedNumber) / 2, guessedNumber, lowerInt);
        }
        if (0 == result) {
            return guessedNumber;
        }
        return 0;
    }

}
