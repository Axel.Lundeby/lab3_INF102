package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        int lastIndex = list.size() - 1;

        if (list.isEmpty()) {
            return 0;
        } else {
            long lastElement = list.get(lastIndex);
            list.remove(lastIndex);
            return lastElement += sum(list);
        }
    }

}
