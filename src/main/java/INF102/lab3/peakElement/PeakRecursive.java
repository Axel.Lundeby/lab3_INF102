package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers.isEmpty())
            return 0;
        int n = numbers.size();

        if (n == 1)
            return numbers.get(0);

        if (numbers.get(0) > numbers.get(1))
            return numbers.get(0);

        if (numbers.get(n - 1) > numbers.get(n - 2))
            return numbers.get(n - 1);

        int prev = numbers.get(n - 3);
        int current = numbers.get(n - 2);
        int next = numbers.get(n - 1);

        if (isPeak(prev, current, next))
            return current;

        numbers.remove(n - 1);
        return peakElement(numbers);
    }

    private boolean isPeak(int prev, int current, int next) {
        return current > prev && current > next;
    }

}
